# Depends neovim => 0.5

The goal of this setup is to be minimalistic and do not interfere with std config folder

The ideas are borrowed from :

- [setup neovim 0.5](https://blog.inkdrop.info/how-to-set-up-neovim-0-5-modern-plugins-lsp-treesitter-etc-542c3d9c9887)
- [install nvim-nightly alongside stable](https://dev.to/creativenull/installing-neovim-nightly-alongside-stable-10d0)
- [nvim-lua-guide](https://github.com/nanotee/nvim-lua-guide)
- [nvim-config Example 1](https://alpha2phi.medium.com/neovim-init-lua-e80f4f136030)
- [nvim-config Example 2](https://alpha2phi.medium.com/neovim-init-lua-e80f4f136030)
- [nvim-config Example 3](https://oroques.dev/notes/neovim-init/)
- [nvim-config Example 4](https://icyphox.sh/blog/nvim-lua/)
- [nvim-config Example 5](https://github.com/siduck76/NvChad)

## Uses Plug.vim as plugin manager

- Home is ~/.config/nvim-nightly
- Data is ~/.local/share/nvim-nightly
- Plugin config from [source](~/.config/nvim-nightly/after/plugin/) folder are auto-loaded by nvim
- Plugins are stored to [source](~/.local/share/nvim-nightly/plug5ed) and [site/ ] folders are loaded by Plug

## nv script

Config directory (~/.config/nvim-nightly) provides a script to lunch nvim - nv.
This script must be simlinked to a dir in PATH variable(I prefer [source](~/bin/nv) link)

# Mapping {{{

## BarBar

```
BufferNext          | <Tab>
BufferPrevious      | <S-Tab>
BufferClose         | <C-Q>
```

## Telescope

```
find_files          | ';f'
live_grep           | ';r'
spellcheck          | ';s'
builtin             | ';t'
buffers             | '\\'
help_tags           | ';;'
```

## nvim-tree

```
Toggle              | '<leader>ee'
FindFile            | '<leader>ef'
Refresh             | '<leader>er'
```

## quickFix

```
older               | '[-'
newer               | ']+'
```

# User

```
Increment           | '+'
Decrement           | '-'
```

## Search selected

```
Backward            | '#'
Forward             | '*'
```

## word under cursor

```
replace             | '<leader>h'
yank-to-line-end    | 'Y'
```

## Tab

```
tabnext             | 'Tab'
tabprev             | 'S-Tab'

CloseBuffer         | 'C-Q'
```

## Split

```
horizontal          | 'ss'
vertical            | 'sv'
```

## Select window

```
left                | '<C-H>'
right               | '<C-L>'
up                  | 'C-K'
down                | '<C-J>'


left                | 'sh'
right               | 'sl'
up                  | 'sk'
down                | 'sj'
```

## Resize window

```
to-left             | '<C-W><Left>'
to-right            | '<C-W><Right>'
up                  | '<C-W><Up>'
down                | '<C-W><Down>'
```

## Move selected lines

```
up                     | 'K'
down                   | 'J'
up                     | '<leader>k'
down                   | '<leader>j'
```

## Spelling

```
spelllang=en_us     | '<leader>ss'
spelllang=ru_ru     | '<leader>sr'
```

## Compiling

```
compiler-script        | '<leader>c'
open-script            | '<leader>p'
md-preview             | '<leader>sp'
```

## Navigating with guides 

```
(n)to <++>             | '<Space><Space>'
```

}}}
