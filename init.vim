"$HOME/.config/nvim-nightly/init.vim
" from: https://dev.to/creativenull/installing-neovim-nightly-alongside-stable-10d0

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Install vim-plug if missing
" if empty(glob('~/.local/share/nvim-nightly/site/autoload/plug.vim'))
"   silent !curl -fLo ~/.local/share/nvim-nightly/site/autoload/plug.vim --create-dirs
"     \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
"   augroup VimPlugAutoInstall
"     autocmd!
"     autocmd VimEnter * PlugInstall --sync | source '~/.config/nvim-nightly/init.vim'  "$MYVIMRC
"   augroup END
" endif
" ========================

" Fundamentals "{{{
" ---------------------------------------------------------------------
lua << EOF
vim.cmd([[
  set runtimepath-=~/.config/nvim
  set runtimepath-=~/.config/nvim/after
  set runtimepath-=~/.local/share/nvim/site
  set runtimepath-=~/.local/share/flatpak/exports/share/nvim/site
  set runtimepath-=~/.local/share/flatpak/exports/share/nvim/site/after
  set runtimepath-=~/.local/share/nvim/site/after
  set packpath-=~/.local/share/nvim/site
  set packpath-=~/.config/nvim
  set packpath-=~/.config/nvim/after
  set packpath-=~/.local/share/nvim/site/after
  set packpath-=~/.local/share/flatpak/exports/share/nvim/site
  set packpath-=~/.local/share/flatpak/exports/share/nvim/site/after

  set runtimepath+=~/.config/nvim-nightly/after
  set runtimepath+=~/.local/share/nvim-nightly/site/after
  set runtimepath^=~/.local/share/nvim-nightly/site
  set runtimepath^=~/.config/nvim-nightly
  set runtimepath+=~/.dotfiles/vim/ultisnips
]])

vim.cmd('runtime ./maps.vim')

require('settings')
require('plug')
require('config.cmp-rc')
require('funcs')

-- Example config in Lua
vim.g.tokyonight_style = "night"
vim.g.tokyonight_italic_functions = true
vim.g.tokyonight_sidebars = { "qf", "vista_kind", "terminal" }

-- Change the "hint" color to the "orange" color, and make the "error" color bright red
vim.g.tokyonight_colors = { hint = "orange", error = "#ff0000" }

-- Load the colorscheme
vim.cmd[[colorscheme tokyonight]]

EOF
" ------------------------
" ------------------------
" ------------------------ {{{2
" init autocmd
"autocmd!
" set script encoding
"scriptencoding utf-8
" stop loading config if it's on tiny or small
"if !1 | finish | endif

"set nocompatible
"set hidden                              " Required to keep multiple buffers open multiple buffers
"set number relativenumber
"syntax enable
"set fileencodings=utf-8,latin
"set encoding=utf-8
"set title
"set background=dark
"set noswapfile
"set nobackup
"set nowritebackup                       " This is recommended by coc
"set conceallevel=0                      " So that I can see `` in markdown files
"set ruler                               " Show the cursor position all the time
"set hlsearch
"set showcmd
"set cmdheight=1
"set laststatus=2
"set scrolloff=10
"set smarttab                            " Makes tabbing smarter will realize you have 2 vs 4
"set expandtab                           " Converts tabs to spaces
"set smartindent                         " Makes indenting smart
"set autoindent                          " Good auto indent
"set signcolumn=yes                      " Always show the signcolumn, otherwise it would shift the text each time
"set complete+=kspell
"set completeopt=menuone,longest
"set incsearch
"set nospell
"set spelllang=en_us,ru_ru
"let loaded_matchparen = 1
"set backupskip=/tmp/*,/private/tmp/*
"set guifont=Ubuntu-Mono-Nerd-Font-Complete-Mono
"set guifont=SourceCodeProForPowerline
"set guifont=Fira\ Code\ Nerd\ Font
" 2}}}
" incremental substitution (neovim) {{{2
"if has('nvim')
"  set inccommand=split
"endif

" Suppress appending <PasteStart> and <PasteEnd> when pasting
"set t_BE=

"set nosc noru nosm
" Don't redraw while executing macros (good performance config)
"set lazyredraw
"set showmatch
" How many tenths of a second to blink when matching brackets
"set mat=2
" Ignore case when searching
"set ignorecase
" Be smart when using tabs ;)
"set smarttab
" indents
"filetype plugin indent on
"set nowrap "No Wrap lines
"set backspace=start,eol,indent
" Finding files - Search down into subfolders
"set path+=**
"set wildignore+=*/node_modules/*

" Turn off paste mode when leaving insert
"autocmd InsertLeave * set nopaste

" Add asterisks in block comments 2}}}
"set formatoptions+=r

"}}}

" Highlights "{{{
" ---------------------------------------------------------------------
"set cursorline
"set cursorcolumn
"" Sets cursor styles
"  underline in normal, line in insert, Block in replace
" set guicursor=n-v-c-sm:hor20,i-ci-ve:ver25-Cursor,r-cr-o:block
" 
" set listchars=tab:»·,nbsp:+,trail:·,extends:→,precedes:←,eol:¬
" 
" " Set cursor line color on visual mode
" highlight Visual cterm=NONE ctermbg=236 ctermfg=NONE guibg=Grey40
" 
" highlight LineNr cterm=none ctermfg=240 guifg=#2b506e guibg=#000000

"}}}

" File types "{{{
" ---------------------------------------------------------------------
" JavaScript
au BufNewFile,BufRead *.es6 setf javascript
" TypeScript
au BufNewFile,BufRead *.tsx setf typescriptreact
" Markdown
au BufNewFile,BufRead *.md set filetype=markdown
" Flow
au BufNewFile,BufRead *.flow set filetype=javascript

autocmd FileType coffee setlocal shiftwidth=2 tabstop=2
autocmd FileType ruby setlocal shiftwidth=2 tabstop=2
autocmd FileType yaml setlocal shiftwidth=2 tabstop=2

set suffixesadd=.js,.es,.jsx,.json,.css,.less,.sass,.styl,.php,.py,.md

"}}}

" Imports "{{{
" ---------------------------------------------------------------------
""" Plugins and vim-plug   {{{

"runtime ./plug.vim
"if has("unix")
"  let s:uname = system("uname -s")
"  " Do Mac stuff
"  if s:uname == "Darwin\n"
"    set clipboard+=unnamedplus
""    runtime ./macos.vim    " if you use macos you can make tjis file and put
""    specifics
"  endif
"endif

"runtime ./maps.vim
"}}}

" Syntax theme "{{{
" ---------------------------------------------------------------------

" true color
"if exists("&termguicolors") && exists("&winblend")
"  syntax enable
"  set termguicolors
"  set winblend=0
"  set wildoptions=pum
"  set pumblend=5
"  set background=dark
"  " Use TokyoNight  there are storm and night dark styles
"  "  runtime ./colors/dracula.vim
"  "  colorscheme dracula
"  " Example config in VimScript
"  let g:tokyonight_style = "night"
"  let g:tokyonight_italic_functions = 1
"  let g:tokyonight_sidebars = [ "qf", "vista_kind", "terminal", "nvim-tree" ]
"
"  " Load the colorscheme
"  colorscheme tokyonight
"endif  }}}

" -------------------------
"  Drop these in your vimrc and you can do :call GetSyntax() when your
"  cursor is over some text to see what highlight settings it belongs to   {{{
function! GetSyntaxID()
    return synID(line('.'), col('.'), 1)
endfunction

function! GetSyntaxParentID()
    return synIDtrans(GetSyntaxID())
endfunction

function! GetSyntax()
    echo synIDattr(GetSyntaxID(), 'name')
    exec "hi ".synIDattr(GetSyntaxParentID(), 'name')
endfunction
" -------------------------
"}}}

" Extras "{{{
" ---------------------------------------------------------------------
set omnifunc=syntaxcomplete#Complete

let vim_better_default_enable_folding=1
if get(g:, 'vim_better_default_enable_folding', 1)
  set foldenable
"  set foldmarker={,}
  set foldlevel=0
  set foldmethod=marker
  " set foldcolumn=3
  set foldlevelstart=99
endif

" Runs a script that cleans out tex build files whenever I close out of a .tex file.
	autocmd VimLeave *.tex !texclear %

" Ensure tabs don't get converted to spaces in Makefiles.
	autocmd FileType make setlocal noexpandtab

" Ensure files are read as what I want:
	autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff
	autocmd BufRead,BufNewFile *.tex set filetype=tex

set exrc
"}}}

" Trigger configuration. You need to change this to something other than <tab> if you use one of the following:  {{{
" - https://github.com/Valloric/YouCompleteMe
" - https://github.com/nvim-lua/completion-nvim
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

let g:UltiSnipsSnippetDirectories=["UltiSnips", "ultisnipps"]

" }}}

" vim: set foldmethod=marker foldlevel=0:

