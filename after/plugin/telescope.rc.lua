if not pcall(require, "telescope") then
  return
end

-- https://github.com/nvim-telescope/telescope.nvim#telescope-defaults
-- Required to close with ESC in insert mode
local actions = require('telescope.actions')
local telescope = require('telescope')
-- Global remapping
-- example keymap:
vim.api.nvim_set_keymap('n', ';f', '<CMD>lua require\'telescope.builtin\'.find_files()<CR>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', ';r', '<CMD>lua require\'telescope.builtin\'.live_grep()<CR>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', ';t', '<CMD>lua require\'telescope.builtin\'.builtin()<CR>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '\\', '<CMD>lua require\'telescope.builtin\'.buffers()<CR>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', ';;', '<CMD>lua require\'telescope.builtin\'.help_tags()<CR>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', ';s', '<CMD>lua require\'telescope.builtin\'.spell_suggest()<CR>', {noremap = true, silent = true})

-- ** the Telescope comma maps **
-- -- find files with names that contain cursor word
vim.api.nvim_set_keymap(
  "n",
  ",f",
  [[<Cmd>lua require'telescope.builtin'.find_files({find_command={'fd', vim.fn.expand('<cword>')}})<CR>]],
  { noremap = true, silent = true  }
)
-- show LSP diagnostics for all open buffers
vim.api.nvim_set_keymap(
  "n",
  ",d",
  [[<Cmd>lua require'telescope.builtin'.lsp_workspace_diagnostics()<CR>]],
    { noremap = true, silent = true  }
)
-- open available commands & run it
vim.api.nvim_set_keymap(
  "n",
  ",c",
  [[<Cmd>lua require'telescope.builtin'.commands({results_title='Commands Results'})<CR>]],
    { noremap = true, silent = true  }
)
-- Telescope oldfiles
vim.api.nvim_set_keymap(
  "n",
  ",o",
  [[<Cmd>lua require'telescope.builtin'.oldfiles({results_title='Recent-ish Files'})<CR>]],
    { noremap = true, silent = true  }
)
-- Telescopic version of FZF's :Lines
vim.api.nvim_set_keymap(
  "n",
  ",l",
  [[<Cmd>lua require('telescope.builtin').live_grep({grep_open_files=true})<CR>]],
    { noremap = true, silent = true  }
)
vim.api.nvim_set_keymap(
  "n",
  ",g",
  [[<Cmd>lua require'telescope.builtin'.live_grep()<CR>]],
    { noremap = true, silent = true  }
)
vim.api.nvim_set_keymap(
  "n",
  ",k",
  [[<Cmd>lua require'telescope.builtin'.keymaps({results_title='Key Maps Results'})<CR>]],
    { noremap = true, silent = true  }
)
vim.api.nvim_set_keymap(
  "n",
  ",b",
  [[<Cmd>lua require'telescope.builtin'.buffers({results_title='Buffers'})<CR>]],
    { noremap = true, silent = true  }
)
vim.api.nvim_set_keymap(
  "n",
  ",h",
  [[<Cmd>lua require'telescope.builtin'.help_tags({results_title='Help Results'})<CR>]],
    { noremap = true, silent = true  }
)
vim.api.nvim_set_keymap(
  "n",
  ",m",
  [[<Cmd>lua require'telescope.builtin'.marks({results_title='Marks Results'})<CR>]],
    { noremap = true, silent = true  }
)
-- End Telescope comma maps
------------------------------

require('telescope').setup{
  defaults = {
    vimgrep_arguments = {
      'rg',
      '--color=never',
      '--no-heading',
      '--with-filename',
      '--line-number',
      '--column',
      '--smart-case'
    },
		prompt_prefix = ' > ',
    selection_caret = " ",
    entry_prefix = "  ",
    initial_mode = "insert",
    selection_strategy = "reset",
    sorting_strategy = "descending",
    layout_strategy = "horizontal",
    path_display = {
      'shorten',
      'absolute',
    },
    layout_config = {
	    prompt_position = 'bottom',
      horizontal = {
        mirror = false,
		    preview_width = 0.6,
      },
      vertical = {
        mirror = false,
      },
    },
    file_sorter =  require'telescope.sorters'.get_fuzzy_file,
    file_ignore_patterns = {},
    generic_sorter =  require'telescope.sorters'.get_generic_fuzzy_sorter,
    winblend = 0,
    border = {},
    borderchars = { '─', '│', '─', '│', '╭', '╮', '╯', '╰' },
    color_devicons = true,
    use_less = true,
    path_display = {},
    set_env = { ['COLORTERM'] = 'truecolor' }, -- default = nil,
    file_previewer = require'telescope.previewers'.vim_buffer_cat.new,
    grep_previewer = require'telescope.previewers'.vim_buffer_vimgrep.new,
    qflist_previewer = require'telescope.previewers'.vim_buffer_qflist.new,

    -- Developer configurations: Not meant for general override
    buffer_previewer_maker = require'telescope.previewers'.buffer_previewer_maker,
        mappings = {
            n = {
                ["q"] = actions.close,
            }
        }
  }
}

