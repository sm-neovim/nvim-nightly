" Add additional dict folders
"let g:vim_dict_dict = [
"    \ '~/.vim/dict',
"    \ '~/.config/nvim-nightly/dict',
"    \ ]

" File type override
let g:vim_dict_config = {'html':'html,javascript,css', 'markdown':'text'}

" Disable certain types
let g:vim_dict_config = {'text': ''}



" Lua code  - doesn't work
lua << EOF
vim.g.vim_dict_dict = {'~/.vim/dict', '~/.config/nvim-nightly/dict'}
--vim.g.vim_dict_config = {'html'='html,javascript,css','markdown'='text'}
--vim.g.vim_dict_dict = {{'~/.config/nvim-nightly/dict','~/.vim/dict'}}
EOF
