if not pcall(require, "nvim-treesitter") then
  return
end

require'nvim-treesitter.configs'.setup {
  highlight = {
    enable = true,
    disable = {},
  },
  indent = {
    enable = false,
    disable = {},
  },
  autotag = {
      enable = true
  },
  ensure_installed = {
    "cpp",
    "go",
    "tsx",
    "toml",
    "bash",
    "php",
    "json",
    "yaml",
    "html",
    "scss"
  },
  ignore_install = { "haskell" }, -- List of parsers to ignore installing
}

local parser_config = require "nvim-treesitter.parsers".get_parser_configs()
parser_config.tsx.used_by = { "javascript", "typescript.tsx" }
