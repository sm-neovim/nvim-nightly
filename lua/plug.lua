
-- """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
vim.cmd([[
 " Install vim-plug if missing
 if empty(glob('~/.local/share/nvim-nightly/site/autoload/plug.vim'))
   silent !curl -fLo ~/.local/share/nvim-nightly/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
   augroup VimPlugAutoInstall
     autocmd!
     autocmd VimEnter * PlugInstall --sync | source '~/.config/nvim-nightly/init.vim'  "$MYVIMRC
   augroup END
 endif
]])
-- " ========================

local Plug = vim.fn['plug#']

vim.call('plug#begin', '~/.local/share/nvim-nightly/plug5ed')

  Plug 'tpope/vim-fugitive'
  Plug 'cohama/lexima.vim'
  Plug 'folke/tokyonight.nvim'
  Plug 'kyazdani42/nvim-web-devicons'
  Plug 'hoob3rt/lualine.nvim'
  Plug 'akinsho/nvim-bufferline.lua'
  Plug 'kyazdani42/nvim-tree.lua'
  Plug('nvim-treesitter/nvim-treesitter', {
    ['do'] = function()
      vim.cmd('TSUpdate')
      end
  })
  Plug 'windwp/nvim-ts-autotag'
  Plug 'nvim-lua/popup.nvim'
  Plug 'nvim-lua/plenary.nvim'
  Plug 'nvim-telescope/telescope.nvim'
  Plug 'skywind3000/vim-dict'
  Plug 'npxbr/glow.nvim'
  Plug 'sudormrfbin/cheatsheet.nvim'
  Plug 'sbdchd/neoformat'
  Plug 'lukas-reineke/indent-blankline.nvim'
  Plug 'voldikss/vim-floaterm'
  Plug 'reedes/vim-pencil'
  Plug 'preservim/vim-lexical'
  Plug 'kana/vim-textobj-user'
  Plug 'preservim/vim-textobj-sentence'
  Plug 'godlygeek/tabular'
  Plug 'plasticboy/vim-markdown'
  Plug 'neovim/nvim-lspconfig'
  Plug 'hrsh7th/cmp-nvim-lsp'
  Plug 'hrsh7th/cmp-buffer'
  Plug 'hrsh7th/nvim-cmp'

--" For vsnip user.
--Plug 'hrsh7th/cmp-vsnip'
--Plug 'hrsh7th/vim-vsnip'

--" For luasnip user.
--" Plug 'L3MON4D3/LuaSnip'
--" Plug 'saadparwaiz1/cmp_luasnip'
--
--" For ultisnips user.
  Plug 'SirVer/ultisnips'
  Plug 'quangnguyen30192/cmp-nvim-ultisnips'

vim.call('plug#end')
--" ========================

-- " vim: set foldmethod=marker foldlevel=0:
