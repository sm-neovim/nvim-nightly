
-- Disable providers we do not care a about
 vim.g.python3_host_prog  = "/usr/bin/python3"
 vim.g.loaded_python_provider  = 0
 vim.g.loaded_perl_provider    = 0
-- vim.g.loaded_ruby_provider    = 0
-- vim.g.loaded_node_provider    = 0

-- Disable some in built plugins completely
 local disabled_built_ins = {
  -- 'netrw', 'netrwPlugin', 'netrwSettings', 'netrwFileHandlers',
   'gzip', 'zip', 'zipPlugin', 'tar', 'tarPlugin',
   'getscript', 'getscriptPlugin','vimball', 'vimballPlugin',
   '2html_plugin', 'logipat', 'rrhelper', 'spellfile_plugin',
   -- 'man','matchit', 'matchparen', 'shada_plugin',
 }
for _, plugin in pairs(disabled_built_ins) do
  vim.g['loaded_' .. plugin] = 1
end

-- Use spelling for markdown files ‘]s’ to find next, ‘[s’ for previous, 'z=‘ for suggestions when on one.
-- Source: http:--thejakeharding.com/tutorial/2012/06/13/using-spell-check-in-vim.html
vim.api.nvim_exec(
  [[
    augroup markdownSpell
        autocmd!
        autocmd FileType markdown,md,txt setlocal spell
        autocmd BufRead,BufNewFile *.md,*.txt,*.markdown setlocal spell
    augroup END
    
    augroup BgHighlight
      autocmd!
      autocmd WinEnter * set cul
      autocmd WinLeave * set nocul
    augroup END
    
    if &term =~ "screen"
      autocmd BufEnter * if bufname("") !~ "^?[A-Za-z0-9?]*://" | silent! exe '!echo -n "\ek[`hostname`:`basename $PWD`/`basename %`]\e\\"' | endif
      autocmd VimLeave * silent!  exe '!echo -n "\ek[`hostname`:`basename $PWD`]\e\\"'
    endif
    
    augroup highlight_yank
        autocmd!
        au TextYankPost * silent! lua vim.highlight.on_yank { higroup='IncSearch', timeout=1000 }
    augroup END

    augroup pencil
        autocmd!
        autocmd FileType markdown,mkd call pencil#init()
        autocmd FileType text         call pencil#init()
      let g:lexical#spelllang = ['en_us','ru_ru',]
    augroup END
  ]],
  false
)

local set = vim.opt

vim.g.title = true
vim.g.hidden = true
vim.g.nocompatible = true
vim.g.scriptencoding = 'utf-8'
vim.g.encoding = 'utf-8'
vim.g.sintax = 'enable'

vim.cmd('set t_BE= ')
-- Set the behavior of tab
set.conceallevel = 0                   --" So that I can see `` in markdown files
set.ruler = true                               --" Show the cursor position all the time
set.lazyredraw = true
set.ignorecase = true
set.incsearch = true
set.hlsearch = true
set.showcmd = true
set.cmdheight = 1
set.laststatus = 2
set.scrolloff = 10
set.smarttab = true                         --  " Makes tabbing smarter will realize you have 2 vs 4
set.tabstop = 4
set.shiftwidth = 4
set.softtabstop = 4
set.expandtab = true
set.smartindent = true
set.autoindent = true
set.wrap = false                    --"No Wrap lines
set.backspace = {'start' ,'eol' , 'indent'}

vim.bo.smartindent = true -- Makes indenting smart
vim.wo.number = true -- set numbered lines
vim.wo.relativenumber = true -- set relative number
vim.wo.cursorline = true -- Enable highlighting of the current line
vim.wo.signcolumn = "yes"
vim.o.showtabline = 2 -- Always show tabs
vim.o.showmode = false -- We don't need to see things like -- INSERT -- anymore
vim.o.backup = false -- This is recommended by coc
vim.o.writebackup = false -- This is recommended by coc
vim.o.splitbelow = true
vim.o.splitright = true
vim.o.termguicolors = true
vim.o.guifont="Ubuntu-Mono-Nerd-Font-Complete-Mono"
vim.o.background = 'dark'

if vim.fn.has('nvim') == 1 then
  vim.o.inccommand = 'split'
end

--vim.opt.listchars = {eol = '↲', tab = '▸ ', trail = '·'}
set.listchars = {tab = '»·', nbsp = '+', trail = '·', extends = '→', precedes = '←', eol = '¬'}
set.wildignore = {'*/cache/*', '*/tmp/*', '*/node_modules/*'}
set.path = {'**'}
set.spell = false
set.complete = {'kspell'}
set.completeopt = {'menuone', 'longest'}
set.spelllang = {'en_us', 'ru_ru'}
--"" Sets cursor styles
--"  underline in normal, line in insert, Block in replace
vim.cmd('set guicursor = "n-v-c-sm:hor20","i-ci-ve:ver25-Cursor","r-cr-o:block"')

--" Set cursor line color on visual mode
vim.cmd('highlight Visual cterm=NONE ctermbg=236 ctermfg=NONE guibg=Grey40')

vim.cmd('highlight LineNr cterm=none ctermfg=240 guifg=#2b506e guibg=#000000')
vim.cmd('filetype plugin on')
vim.cmd('set formatoptions+=r')
vim.cmd('set whichwrap+=<,>,[,],h,l')

