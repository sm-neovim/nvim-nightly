lua << EOF
-- toggle foldcolumn
vim.api.nvim_set_keymap(
  "n",
  ",tf",
  ":lua require'funcs'.toggleFoldCol()<CR>",
  { noremap = true, silent = true }
  ) 

  -- toggle IndentBlankline with set line! - both off by default
vim.api.nvim_set_keymap("n", ",ti", ":IndentBlanklineToggle<CR>:set list!<CR>", { noremap = true, silent = true })
EOF
"
"
" Description: Keymaps
" show/hide unprintable characters
nnoremap <Leader>sc :let &cole=(&cole == 2) ? 0 : 2 <bar> echo 'conceallevel ' . &cole <CR>

" inverts display of unprintable characters
nnoremap <silent> <leader>l :set list! list?<CR>

" Yank-Past from OS clipboard
nnoremap <S-y> "+y
nnoremap <S-p> "+p
" Delete without yank
nnoremap <leader>d "_d
nnoremap x "_x

" Increment/decrement
nnoremap + <C-a>
nnoremap - <C-x>

" Save with root permission
command! W w !sudo tee > /dev/null %

" Search for selected text, forwards or backwards.
vnoremap <silent> * :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy/<C-R><C-R>=substitute(
  \escape(@", '/\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>
vnoremap <silent> # :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy?<C-R><C-R>=substitute(
  \escape(@", '?\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>

"-----------------------------
" Tabs

" Open current directory
nmap te :tabedit
nmap <S-Tab> :tabprev<Return>
nmap <Tab> :tabnext<Return>

"------------------------------
" Windows
  " close pane using <C-Q>
  " not use the <C-q> mappings anyway
  function! <SID>CloseBuffer()
    if len(getwininfo()) >? 1
      close
    elseif len(getbufinfo()) >? 1
      bdelete
    endif
  endfunction
  nnoremap <silent> <C-Q> :call <SID>CloseBuffer()<Cr>

" Split window
nmap ss :split<Return><C-w>w
nmap sv :vsplit<Return><C-w>w
" Move window
map <C-H> <C-w>h
map <C-K> <C-w>k
map <C-J> <C-w>j
map <C-l> <C-w>l
map sh <C-w>h
map sk <C-w>k
map sj <C-w>j
map sl <C-w>l
" Resize window
nmap <C-w><left> <C-w><
nmap <C-w><right> <C-w>>
nmap <C-w><up> <C-w>+
nmap <C-w><down> <C-w>-


" map <leader>o :setlocal spell! spelllang=en_us<CR>
	map <leader>ss :setlocal spell! spelllang=en_us<CR>
	map <leader>sr :setlocal spell! spelllang=ru_ru<CR>

" Compile document, be it groff/LaTeX/markdown/etc.
	map <leader>c :w! \| !compiler <c-r>%<CR>

" Open corresponding .pdf/.html or preview
	map <leader>p :!opout <c-r>%<CR><CR>

" Navigating with guides <+++>
	"inoremap <Space><Space> <Esc>/i<Enter>"_c4l
	"vnoremap <Space><Space> <Esc>/<Enter>"_c4l
	map <Space><Space> <Esc>/<++><Enter>"_c4l

" replace word under cursor, globally, with confirmation
  nnoremap <leader>h :%s/\<<C-r><C-w>\>//gc<Left><Left><Left>
  vnoremap <leader>h y :%s/<C-r>"//gc<Left><Left><Left>

" move lines around
  vnoremap J :m '>+1<CR>gv=gv
  vnoremap K :m '<-2<CR>gv=gv
  nnoremap <leader>k :m-2<cr>==
  nnoremap <leader>j :m+<cr>==
  xnoremap <leader>k :m-2<cr>gv=gv
  xnoremap <leader>j :m'>+<cr>gv=gv

  nmap <leader>sf :Neoformat<CR>
  nmap <leader>sp :Glow<CR>

  " yank till end of the line  
  nnoremap Y y$

  " Keeping it centered
  nnoremap n nzzzv
  nnoremap N Nzzzv
  nnoremap J mzJ'z
 
"--------MARKDOWN {{{
	autocmd Filetype markdown,rmd,md inoremap ,n ---<Enter><Enter>
	autocmd Filetype markdown,rmd,md inoremap ,b ****<++><Esc>F*hi
	autocmd Filetype markdown,rmd,md inoremap ,s ~~~~<++><Esc>F~hi
	autocmd Filetype markdown,rmd,md inoremap ,e **<++><Esc>F*i
	autocmd Filetype markdown,rmd,md inoremap ,h ====<Space><++><Esc>F=hi
	autocmd Filetype markdown,rmd,md inoremap ,i ![](<++>)<++><Esc>F[a
	autocmd Filetype markdown,rmd,md inoremap ,a [](<++>)<++><Esc>F[a
	autocmd Filetype markdown,rmd,md inoremap ,1 #<Space><Enter><++><Esc>kA
	autocmd Filetype markdown,rmd,md inoremap ,2 ##<Space><Enter><++><Esc>kA
	autocmd Filetype markdown,rmd,md inoremap ,3 ###<Space><Enter><++><Esc>kA
	autocmd Filetype markdown,rmd,md inoremap ,l --------<Enter>
" }}}

"--------HTML {{{
	autocmd FileType html inoremap ,b <b></b><Space><++><Esc>FbT>i
	autocmd FileType html inoremap ,it <em></em><Space><++><Esc>FeT>i
	autocmd FileType html inoremap ,1 <h1></h1><Enter><Enter><++><Esc>2kf<i
	autocmd FileType html inoremap ,2 <h2></h2><Enter><Enter><++><Esc>2kf<i
	autocmd FileType html inoremap ,3 <h3></h3><Enter><Enter><++><Esc>2kf<i
	autocmd FileType html inoremap ,p <p></p><Enter><Enter><++><Esc>02kf>a
	autocmd FileType html inoremap ,a <a<Space>href=""><++></a><Space><++><Esc>14hi
	autocmd FileType html inoremap ,e <a<Space>target="_blank"<Space>href=""><++></a><Space><++><Esc>14hi
	autocmd FileType html inoremap ,ul <ul><Enter><li></li><Enter></ul><Enter><Enter><++><Esc>03kf<i
	autocmd FileType html inoremap ,li <Esc>o<li></li><Esc>F>a
	autocmd FileType html inoremap ,ol <ol><Enter><li></li><Enter></ol><Enter><Enter><++><Esc>03kf<i
	autocmd FileType html inoremap ,im <img src="" alt="<++>"><++><esc>Fcf"a
	autocmd FileType html inoremap ,td <td></td><++><Esc>Fdcit
	autocmd FileType html inoremap ,tr <tr></tr><Enter><++><Esc>kf<i
	autocmd FileType html inoremap ,th <th></th><++><Esc>Fhcit
	autocmd FileType html inoremap ,tab <table><Enter></table><Esc>O
	autocmd FileType html inoremap ,gr <font color="green"></font><Esc>F>a
	autocmd FileType html inoremap ,rd <font color="red"></font><Esc>F>a
	autocmd FileType html inoremap ,yl <font color="yellow"></font><Esc>F>a
	autocmd FileType html inoremap ,dt <dt></dt><Enter><dd><++></dd><Enter><++><esc>2kcit
	autocmd FileType html inoremap ,dl <dl><Enter><Enter></dl><enter><enter><++><esc>3kcc
	autocmd FileType html inoremap &<space> &amp;<space>
" }}}
