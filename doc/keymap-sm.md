# Mapping

## BarBar

```
BufferNext          | <Tab>
BufferPrevious      | <S-Tab>
BufferClose         | <S-x>
```

## Telescope

```
find_files          | ';f'
live_grep           | ';r'
builtin             | ';t'
buffers             | '\\'
help_tags           | ';;'
```

## nvim-tree

```
Toggle              | '<leader>ee'
FindFile            | '<leader>ef'
Refresh             | '<leaderer>'
```

## quickFix

```
older               | '[-'
newer               | ']+'
```

# User

```
Increment           | '+'
Decrement           | '-'
Select_all          | '<C-a>'
```

## Search selected

```
Backward            | '#'
Forward             | '*'
```

## word under cursor

```
replace             | '<leader>h'
```

## Tab

```
tabnext             | 'Tab'
tabprev             | 'S-Tab'

CloseBuffer         | 'C-Q'
```

## Split

```
horizontal          | 'ss'
vertical            | 'sv'
```

## Select window

```
left                | '<C-H>'
right               | '<C-L>'
up                  | 'C-K'
down                | '<C-J>'

left                | 'sh'
right               | 'sl'
up                  | 'sk'
down                | 'sj'
```

## Resize window

```
to-left             | '<C-W><Left>'
to-right            | '<C-W><Right>'
up                  | '<C-W><Up>'
down                | '<C-W><Down>'
```

## Move selected lines

```
up                     | '<leader>k'
down                   | '<leader>j'
```

## Spelling

```
spelllang=en_us     | '<leader>ss'
spelllang=ru_ru     | '<leader>sr'
```

## Compiling

```
compiler-script        | '<leader>c'
open-script            | '<leader>p'
md-preview             | '<leader>sp'
preer-format           | '<leader>sf'
```

## Cheatsheet

```
openinTelescope        | '<leader><Shift>?'
```

## Navigating with guides <++>

```
(n)to <++>             | '<Space><Space>'
```

